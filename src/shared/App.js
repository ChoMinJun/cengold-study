import { BrowserRouter, Route, Redirect } from "react-router-dom";
import "@progress/kendo-theme-bootstrap/dist/all.css";
import DrawerContainer from "../components/DrawerContainer";
import { MENU } from "../pages";

const App = () => {
  const [
    {
      path: indexDepth1,
      pages: [{ path: indexDepth2 }],
    },
  ] = MENU;

  const indexPath = indexDepth1 + indexDepth2;

  return (
    <BrowserRouter>
        <DrawerContainer>
          <Route exact path="/">
            <Redirect to={indexPath} />
          </Route>
          {MENU.map(({ path: depth1, pages }) => {
            return pages.map(({ path: depth2, component }) => {
              const path = depth1 + depth2;
              return <Route key={path} path={path} component={component} />;
            });
          })}
        </DrawerContainer>
    </BrowserRouter>
  );
};

export default App;
