import React from "react";
import { Button as KendoButton } from "@progress/kendo-react-buttons";

const Button = (props) => {
  return <KendoButton look="outline" primary {...props} />;
};

export default Button;
