import SyntaxHighlighter from "react-syntax-highlighter";
import { docco } from "react-syntax-highlighter/dist/esm/styles/hljs";

const CodeBlock = ({ language, code, children }) => {
  return (
    <SyntaxHighlighter language={language} style={docco}>
      {code || children}
    </SyntaxHighlighter>
  );
};

export default CodeBlock;
