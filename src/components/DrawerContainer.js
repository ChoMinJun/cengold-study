import { useState } from "react";
import { withRouter } from "react-router-dom";
import {
  Drawer,
  DrawerContent,
  AppBar,
  AppBarSection,
  AppBarSpacer,
} from "@progress/kendo-react-layout";
import { MENU } from "../pages";

const DrawerContainer = (props) => {
  const [expanded, setExpanded] = useState(false);

  const handleClickChapter = (path) => {
    return () => {
      props.history.push(path);
    };
  };

  const handleClickMenu = () => {
    setExpanded(!expanded);
  };

  const onSelect = (e) => {
    props.history.push(e.itemTarget.props.route);
  };

  const setSelectedItem = () => {
    const pathArray = props.location.pathname.split("/");
    const chapter = "/" + pathArray[1];
    const page = "/" + pathArray[2];
    return { chapter, page };
  };

  let { chapter, page } = setSelectedItem();

  const chapterList = MENU.map(({ title, path: depth1, pages }, index) => {
    let depth2 = "";
    if (pages.length) {
      depth2 = pages[0].path;
    }
    return (
      <li
        key={index}
        className={depth1 === chapter ? "selected" : ""}
        onClick={handleClickChapter(depth1 + depth2)}
      >
        <span>{title}</span>
      </li>
    );
  });

  const selectedChapter = MENU.find(({ path }) => path === chapter) || {};
  const { path: depth1, pages = [] } = selectedChapter;
  const pageList = pages.map(({ title, path: depth2 }) => {
    // Icon docs https://www.telerik.com/kendo-react-ui/components/styling/icons/#toc-list-of-font-icons
    return {
      text: title,
      icon: "k-i-more-vertical",
      route: depth1 + depth2,
    };
  });

  return (
    <div>
      <AppBar>
        <AppBarSection>
          <button className="k-button k-button-clear" onClick={handleClickMenu}>
            <span className="k-icon k-i-menu" />
          </button>
        </AppBarSection>
        <AppBarSpacer style={{ width: 1 }} />
        <AppBarSection style={{ width: 230 }}>
          <h1 className="title">KendoReact</h1>
        </AppBarSection>

        <AppBarSpacer style={{ width: 32 }} />

        <AppBarSection>
          <ul>{chapterList}</ul>
        </AppBarSection>

        <AppBarSpacer />
        <AppBarSection>{/* Signed ID : ExampleID */}</AppBarSection>
        <AppBarSpacer style={{ width: 50 }} />
        <AppBarSection>{/* Sign In || Sign Out */}</AppBarSection>
      </AppBar>

      <Drawer
        expanded={expanded}
        position={"start"}
        mode={"push"}
        mini={true}
        width={200}
        items={pageList.map((item) => ({
          ...item,
          selected: item.route === chapter + page,
        }))}
        onSelect={onSelect}
      >
        <DrawerContent>
          <div className="content-wrap">{props.children}</div>
        </DrawerContent>
      </Drawer>
    </div>
  );
};

export default withRouter(DrawerContainer);
