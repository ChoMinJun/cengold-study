import React from "react";
import { Input } from "@progress/kendo-react-inputs";

const TextInput = (props) => {
  return <Input {...props} />;
};

export default TextInput;
