import ES6Array from "./chapter01/ES6Array";
import ES6Object from "./chapter01/ES6Object";
import Question01 from "./chapter01/Question01";

import ReduxIntroduction from "./chapter02/ReduxIntroduction";
import ReduxDucksPattern from "./chapter02/ReduxDucksPattern";
import ReduxHooks from "./chapter02/ReduxHooks";
import ReduxTutorial from "./chapter02/ReduxTutorial";
import Question02 from "./chapter02/Question02";

export const MENU = [
  {
    title: "Chapter01",
    path: "/chapter01",
    pages: [
      { title: "ES6Array", path: "/es6-array", component: ES6Array },
      { title: "ES6Object", path: "/es6-object", component: ES6Object },
      { title: "Question01", path: "/question01", component: Question01 },
    ],
  },
  {
    title: "Chapter02",
    path: "/chapter02",
    pages: [
      {
        title: "Redux Introduction",
        path: "/redux-introduction",
        component: ReduxIntroduction,
      },
      {
        title: "Redux Ducks Pattern",
        path: "/redux-ducks-pattern",
        component: ReduxDucksPattern,
      },
      {
        title: "Redux Hooks",
        path: "/redux-hooks",
        component: ReduxHooks,
      },
      {
        title: "Redux Tutorial",
        path: "/redux-tutorial",
        component: ReduxTutorial,
      },
      {
        title: "Question02",
        path: "/question02",
        component: Question02,
      },
    ],
  },
];
