import React, { useRef } from "react";
import CodeBlock from "../../components/CodeBlock";
import reactDataFlow from "../../images/reactDataFlow.png";
import reduxStateTransfer from "../../images/reduxStateTransfer.jpeg";
import reduxTrends from "../../images/reduxTrends.png";

const ReduxIntroduction = () => {
  const focusTarget = useRef([]);

  const handleClickScrollSection = (i) => {
    return () => {
      focusTarget.current[i].scrollIntoView({ behavior: "smooth" });
    };
  };

  return (
    <div>
      <div className="section-list">
        <ul>
          <li onClick={handleClickScrollSection(0)}>
            React에 상태 관리 라이브러리가 왜 필요한가?
          </li>
          <li onClick={handleClickScrollSection(1)}>Redux 란?</li>
          <ul>
            <li onClick={handleClickScrollSection(2)}>단점</li>
            <li onClick={handleClickScrollSection(3)}>기본 구조</li>
            <li onClick={handleClickScrollSection(4)}>기본 사용법</li>
          </ul>
        </ul>
      </div>

      <h1 className="scroll-margin" ref={(el) => (focusTarget.current[0] = el)}>
        React에 상태 관리 라이브러리가 왜 필요한가?
      </h1>
      <ul>
        <li>
          <h3>Local State의 전달이 어렵다.</h3>
          <img src={reactDataFlow} alt="" />
        </li>
        <li>
          <h3>Global State의 전달이 어렵다.</h3>
          <h4>
            테마, 선호하는 언어, 로그인한 유저의 인증정보, 결제정보, 개인정보 등
            전역에 필요한 상태값의 전달이 위의 diagram보다 훨씬 복잡한 절차를
            걸치게 될것이다.
          </h4>
        </li>
        <li>
          <h3>대표적인 상태 관리 라이브러리</h3>
          <h4>Context API, Redux, MobX, Recoil</h4>
          <img src={reduxTrends} alt="" style={{ width: "80vw" }} />
        </li>
      </ul>

      <h1 className="scroll-margin" ref={(el) => (focusTarget.current[1] = el)}>
        Redux 란?
      </h1>
      <img src={reduxStateTransfer} alt="" />
      <div className="ml30">
        <h2
          className="scroll-margin"
          ref={(el) => (focusTarget.current[2] = el)}
        >
          단점
        </h2>
        <ul>
          <li>
            <h3>
              한가지의 기능을 만들기 위해 작성해야 하는 코드의 양이 불필요하게
              많다. (ActionType, Action, Reducer)
            </h3>
          </li>
          <li>
            <h3>
              사용법이 까다롭다.(connect, mapStateToProps, mapDispatchToProps)
            </h3>
          </li>
          <li>
            <h3>
              이로 인한 React의 진입장벽이 높고 러닝커브가 높다. (사실상 React
              자체는 접근하기 쉬우나 React와 필수 Pair로 불리우는 Redux로 인해
              접근에 어려움을 겪음)
            </h3>
          </li>
        </ul>
      </div>

      <div className="ml30">
        <h2
          className="scroll-margin"
          ref={(el) => (focusTarget.current[3] = el)}
        >
          기본 구조
        </h2>
        <ul>
          <li>
            <h3>Action</h3>
            <CodeBlock
              language="javascript"
              code={`
// src/actions/todo.js

// Action Type
// 일반적으로 문자열 상수로 정의
export const ADD_TODO = 'ADD_TODO';

// Action 생성자
// Action type은 Action Creator를 통해 사용된다.
export const addTodo = (payload) => {
    return { type: ADD_TODO, payload};
}`}
            />
          </li>
          <li>
            <h3>Reducer</h3>
            <CodeBlock
              language="javascript"
              code={`
// src/reducers/todo.js
import { ADD_TODO } from '../actions/todo';

// 어떠한 Action이 발생 하였을 때 해당 Action으로 인하여 어플리케이션의 상태가 어떻게 바뀌는지 정의
const initialState = { filter:[], list:[] };
const todoReducer = (state = initialState, action) {
    switch (action.type) {
        case ADD_TODO:
            const prevList = state.list;
            return {
                ...state,
                list : [...prevList,action.payload]
            }
        default:
            return state;
    }
  }`}
            />
          </li>
          <li>
            <h3>Store</h3>
            <CodeBlock
              language="javascript"
              code={`
// src/index.js
import { createStore } from 'react-redux';
import reducers from './reducers';

// Reducer를 저장하는 어플리케이션
const store = createStore(reducers);`}
            />
          </li>
        </ul>
      </div>
      <div className="ml30">
        <h2
          className="scroll-margin"
          ref={(el) => (focusTarget.current[4] = el)}
        >
          기본 사용법
        </h2>
        <CodeBlock
          language="javascript"
          code={`
import React from "react";
import { connect } from "react-redux";
import { addTodo } from "../actions/todo";

const ToDoList = (props) => {
  const todoList = props.todoList;
  const handleClickAddTodo = (value) => {
    props.onAddTodo(value);
  };
  
  return <div className="todo-list">...</div>;
};

const mapStateToProps = (state) => {
  return { todoList: state.todo.list };
};

const mapDispatchToProps = (dispatch) => {
  return { onAddTodo: (payload) => dispatch(addTodo(payload)) };
};

export default connect(mapStateToProps, mapDispatchToProps)(ToDoList);
`}
        />
      </div>
    </div>
  );
};

export default ReduxIntroduction;
