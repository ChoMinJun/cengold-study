import React, { useRef } from "react";
import CodeBlock from "../../components/CodeBlock";

const ReduxDucksPattern = () => {
  const focusTarget = useRef([]);

  const handleClickScrollSection = (i) => {
    return () => {
      focusTarget.current[i].scrollIntoView({ behavior: "smooth" });
    };
  };

  return (
    <div>
      <div className="section-list">
        <ul>
          <li onClick={handleClickScrollSection(0)}>Ducks Pattern 이란?</li>
          <ul>
            <li onClick={handleClickScrollSection(1)}>어원</li>
            <li onClick={handleClickScrollSection(2)}>규칙</li>
            <li onClick={handleClickScrollSection(3)}>예시</li>
          </ul>
          <li onClick={handleClickScrollSection(4)}>Redux Toolkit</li>
          <ul>
            <li onClick={handleClickScrollSection(5)}>createSlice</li>
          </ul>
        </ul>
      </div>

      <h1 className="scroll-margin" ref={(el) => (focusTarget.current[0] = el)}>
        Ducks Pattern 이란?
      </h1>
      <div
        style={{
          backgroundColor: "lightGray",
          width: "800px",
          padding: "10px",
          border: "1px solid gray",
          borderRadius: 5,
        }}
      >
        Redux를 사용하는 어플리케이션을 구축하다 보면 기능별로 여러 개의 Action
        Types 와, Actions, Reducer 한 세트를 만들어야 한다.
        <br />
        <br />
        이들은 관습적으로 여러 개의 폴더로 나누어져서, 하나의 기능을 수정할 때는
        이 기능과 관련된 여러 개의 파일을 수정해야 하는 일이 생긴다.
        <br />
        <br />
        여기서 불편함을 느껴 나온 것이{" "}
        <span style={{ color: "blue", fontWeight: "bold" }}>
          Ducks Pattern
        </span>{" "}
        이다.
        <br />
        <br />
        ActionTypes, Actions, Reducer를 한 곳으로 모아 하나의 모듈로 관리하자는
        것이 Ducks Pattern의 핵심입니다.
      </div>
      <div className="ml30">
        <h3
          className="scroll-margin"
          ref={(el) => (focusTarget.current[1] = el)}
        >
          어원
        </h3>
        Java has jars and beans. Ruby has gems. I suggest we call these reducer
        bundles “ducks”, as in the last syllable of “redux”.
        <br />
        Java에는 jars와 beans가 있습니다. Ruby에는 gems가 있습니다. "redux"의
        마지막 음절에서와 같이 이 reducer bundles 을 "ducks"라고 부르는 것이
        좋습니다.
        <h3
          className="scroll-margin"
          ref={(el) => (focusTarget.current[2] = el)}
        >
          규칙
        </h3>
        <ol>
          <li>MUST export default a function called reducer()</li>
          <li>MUST export its action creators as functions</li>
          <li>
            MUST have action types in the form
            npm-module-or-app/reducer/ACTION_TYPE
          </li>
          <li>
            MAY export its action types as UPPER_SNAKE_CASE, if an external
            reducer needs to listen for them, or if it is a published reusable
            library
          </li>
        </ol>
        <ol>
          <li>항상 "reducer()"" 란 이름의 함수를 "export default" 해야한다.</li>
          <li>항상 모듈의 "action 생성자"들을 함수형태로 "export" 해야한다.</li>
          <li>
            항상 "npm-module-or-app/reducer/ACTION_TYPE" 형태의 "action 타입"을
            가져야한다.
          </li>
          <li>
            어쩌면 action 타입들을 "UPPER_SNAKE_CASE" 로 "export" 할 수 있다.
          </li>
        </ol>
        <h3
          className="scroll-margin"
          ref={(el) => (focusTarget.current[3] = el)}
        >
          예시
        </h3>
        <CodeBlock language="javascript">{`
// todoList.js

// Actions
const ADD_TODO = 'my-app/todoList/ADD_TODO';
const EDIT_TODO = 'my-app/todoList/EDIT_TODO';
const REMOVE_TODO = 'my-app/todoList/REMOVE_TODO';

// Reducer
export default function reducer(state = {}, action = {}) {
    switch (action.type) {
        case ADD_TODO:
          return state;

        case EDIT_TODO:
          return state; 

        case REMOVE_TODO:
          return state; 

        default: return state;
    }
}

// Action 생성자
export function addTodo(payload) {
    return { type: ADD_TODO, payload };
}

export function editTodo(payload) {
    return { type: EDIT_TODO, payload };
}

export function removeTodo(payload) {
  return { type: REMOVE_TODO, payload };
}
        `}</CodeBlock>
      </div>

      <h1 className="scroll-margin" ref={(el) => (focusTarget.current[4] = el)}>
        Redux Toolkit
      </h1>
      <h2>Redux 에서 공식적으로 지원하는 Redux 개발 도구</h2>
      <div className="ml30">
        <h3
          className="scroll-margin"
          ref={(el) => (focusTarget.current[5] = el)}
        >
          createSlice
        </h3>
        <ul>
          <li>
            Redux 에서 모듈을 관리하기 위한 패턴 중 ducks-pattern을 공식적으로
            지원하기 위해 나옴
          </li>
          <li>
            reducer 만 생성하여도 reducer의 key 값으로 액션까지 자동으로 생성해
            주는 기능을 지원
          </li>
        </ul>
        <h3>예시</h3>
        <CodeBlock language="javascript">
          {`
import { createSlice } from '@reduxjs/toolkit'

const todoList = createSlice({
  name: 'todoList',
  initialState: [],
  reducers: {
    addTodo(state, action) {
      // code
    },
    editTodo(state, action) {
      // code
    },
    removeTodo(state, action) {
      // code
    }
  }
});

export const { addTodo, editTodo, removeTodo } = todoList.actions;

export default todoList.reducer;
`}
        </CodeBlock>
      </div>
    </div>
  );
};

export default ReduxDucksPattern;
