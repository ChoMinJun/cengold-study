import React from "react";
import CodeBlock from "../../components/CodeBlock";

const ReduxHooks = () => {
  return (
    <div>
      <h1>Redux Hooks 란?</h1>
      <div className="ml30">
        <div
          style={{
            backgroundColor: "lightGray",
            width: "800px",
            padding: "10px",
            border: "1px solid gray",
            borderRadius: 5,
          }}
        >
          <h3>React에서 Redux를 사용하기 위해서는...</h3>
          <h4>
            <ol>
              <li>고차 컴포넌트 "connect()" 로 컴포넌트 감싸기</li>
              <li>"mapStateToProps()" 작성</li>
              <li>"mapDispatchToProps()" 작성</li>
            </ol>
          </h4>
          <h3>
            위 과정을 거쳐야만 Redux Store에 접근 할 수 있었다.
            <br />
            <br />
            Redux에서 Hooks를 지원하게 되면서 이러한 작업 없이
            <br />
            간결하고, 코드 재사용성에 유리하게 코드를 작성 할 수 있게 되었다.
          </h3>
        </div>
        <h2>주로 사용 할 hooks</h2>
        <h4>
          <ol>
            <li>mapStateToProps() => useSelector()</li>
            <li>mapDispatchToProps() => useDispatch()</li>
          </ol>
        </h4>
        <br />

        <h3>Did not use a hooks</h3>
        <CodeBlock language="javascript">
          {`
import React from "react";
import { connect } from "react-redux";
import { addTodo } from "../actions/todo";

const ToDoList = (props) => {
    const todoList = props.todoList;
    const handleClickAddTodo = (value) => {
        props.onAddTodo(value);
    };

    return <div className="todo-list">...</div>;
};

const mapStateToProps = (state) => {
    return { todoList: state.todoList };
};

const mapDispatchToProps = (dispatch) => {
    return { onAddTodo: (payload) => dispatch(addTodo(payload)) };
};

export default connect(mapStateToProps, mapDispatchToProps)(ToDoList);
`}
        </CodeBlock>

        <h3>Use a hooks</h3>
        <CodeBlock language="javascript">
          {`
import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { addTodo } from "../actions/todo";

const ToDoList = () => {
    const dispatch = useDispatch();

    const todoList = useSelector(state => state.todoList);
    const handleClickAddTodo = (value) => {
        dispatch(addTodo(value));
    };

    return <div className="todo-list">...</div>;
};

export default ToDoList;
`}
        </CodeBlock>
      </div>
    </div>
  );
};

export default ReduxHooks;
