import React from "react";
import CodeBlock from "../../components/CodeBlock";

const ReduxTutorial = () => {
  return (
    <div>
      <h1>Redux를 이용한 To-do list</h1>
      <ul>
        <li>
          <h3>
            redux를 사용하기 위해 react-redux, @reduxjs/toolkit install
            <br />
            => npm i react-redux @reduxjs/toolkit
          </h3>
        </li>
        <li>
          <h3>To-do list 화면 작성</h3>
          <CodeBlock language="javascript">
            {`
// src/pages/chapter02/ToDoList.js

import React, { useState } from "react";
import TextInput from "../../components/TextInput";
import Button from "../../components/Button";

const ToDoList = () => {
  const [todo, setTodo] = useState("");

  const handleChangeTodo = ({ value }) => {
    setTodo(value);
  };

  const handleClickAdd = () => {
    // Add Event 작성
  };

  const handleClickRemove = () => {
    // Remove Event 작성
  };

  return (
    <div>
      <h1>To-Do List</h1>
      <div style={styles.inputSection}>
        <TextInput
          style={styles.textInput}
          label="To-Do"
          value={todo}
          onChange={handleChangeTodo}
        />

        <Button style={styles.addButton} onClick={handleClickAdd}>
          ADD
        </Button>
      </div>
      <hr />
      <div>
        <div style={styles.todoEachSection}>
          <span>To Do Template</span>
          <Button style={styles.removeButton} onClick={handleClickRemove}>
            REMOVE
          </Button>
        </div>
      </div>
    </div>
  );
};

const styles = {
  inputSection: { position: "relative" },
  textInput: { width: 400 },
  addButton: { marginLeft: 10, position: "absolute", bottom: 0 },
  todoEachSection: {
    marginTop: 10,
    padding: "5px 0",
    width: 500,
    height: 30,
    borderBottom: "1px solid",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  removeButton: { height: "100%" },
};

export default ToDoList;

`}
          </CodeBlock>
        </li>

        <li>
          <h3>module 작성</h3>
          <CodeBlock language="javascript">
            {`
// src/modules/todo.js

import { createSlice } from "@reduxjs/toolkit";

const initialState = { list: [] };

const todoSlice = createSlice({
  name: "todo",
  initialState,
  reducers: {
    addToDo(state, action) {
      state.list.push(action.payload);
    },
    removeToDo(state, action) {
      state.list.splice(action.payload, 1);
    },
  },
});

export const { addToDo, removeToDo } = todoSlice.actions;
export default todoSlice.reducer;
`}
          </CodeBlock>
        </li>

        <li>
          <h3>rootReducer 작성</h3>
          <CodeBlock language="javascript">
            {`
// src/modules/index.js

import { combineReducers } from "redux";
import todo from "./todo";

const rootReducer = combineReducers({ todo });

export default rootReducer;
`}
          </CodeBlock>
        </li>

        <li>
          <h3>store 작성 및 연결</h3>
          <CodeBlock language="javascript">
            {`
// src/index.js

import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./shared/App";
import reportWebVitals from "./reportWebVitals";

import { configureStore } from "@reduxjs/toolkit";
import rootReducer from "./modules";
import { Provider } from "react-redux";

const store = configureStore({ reducer: rootReducer });

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
`}
          </CodeBlock>
        </li>

        <li>
          <h3>Component와 Store 연결 및 로직 추가</h3>
          <CodeBlock language="javascript">
            {`
// src/pages/chapter02/ToDoList.js

import React, { useState } from "react";
import TextInput from "../../components/TextInput";
import Button from "../../components/Button";
import { useSelector, useDispatch } from "react-redux";
import { addToDo, removeToDo } from "../../modules/todo";

const ToDoList = () => {
  const dispatch = useDispatch();
  const todoList = useSelector((state) => state.todo.list);

  const [todo, setTodo] = useState("");

  const handleChangeTodo = ({ value }) => {
    setTodo(value);
  };

  const handleClickAdd = () => {
    dispatch(addToDo(todo));
    setTodo("");
  };

  const handleClickRemove = (index) => {
    return () => {
      dispatch(removeToDo(index));
    };
  };

  return (
    <div>
      <h1>To-Do List</h1>
      <div style={styles.inputSection}>
        <TextInput
          style={styles.textInput}
          label="To-Do"
          value={todo}
          onChange={handleChangeTodo}
        />

        <Button style={styles.addButton} onClick={handleClickAdd}>
          ADD
        </Button>
      </div>
      <hr />
      <div>
        {todoList.map((todo, index) => {
          return (
            <div key={index} style={styles.todoEachSection}>
              <span>{todo}</span>
              <Button
                style={styles.removeButton}
                onClick={handleClickRemove(index)}
              >
                REMOVE
              </Button>
            </div>
          );
        })}
      </div>
    </div>
  );
};

const styles = {
  inputSection: { position: "relative" },
  textInput: { width: 400 },
  addButton: { marginLeft: 10, position: "absolute", bottom: 0 },
  todoEachSection: {
    marginTop: 10,
    padding: "5px 0",
    width: 500,
    height: 30,
    borderBottom: "1px solid",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  removeButton: { height: "100%" },
};

export default ToDoList;
`}
          </CodeBlock>
        </li>
      </ul>
    </div>
  );
};

export default ReduxTutorial;
