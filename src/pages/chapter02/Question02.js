import loginState from "../../images/loginState.png";
import logoutState from "../../images/logoutState.png";
import loginHistory from "../../images/loginHistory.png";

const Question02 = () => {
  return (
    <div>
      <h1>Question 02</h1>

      <hr />

      <h3>1. Chapter02 의 첫 번째 메뉴에 Sign In 페이지 작성</h3>
      <ul>
        <li>
          <h4>Login 상태</h4>
          로그인 상태입니다.
          <br />
          Signed ID : "Login한 아이디"
        </li>
        <li>
          <h4>Logout 상태</h4>
          ID 입력 인풋, Sign In 버튼
          <br />
          Sign In 버튼 클릭 시 signIn Action 발생시켜서 로그인
        </li>
      </ul>

      <hr />

      <h3>2. 상단 네비게이터의 우측에 Sign In 영역 작성</h3>
      <ul>
        <li>
          <h4>Login 상태</h4>
          1. Signed ID : "Login한 아이디"
          <br />
          2. Sign Out 영역 작성 (li 태그)
          <br />
          Sign Out 클릭 시 signOut Action 발생시켜서 로그아웃
        </li>
        <li>
          <h4>Logout 상태</h4>
          Sign In 영역 작성 (li 태그)
          <br />
          Sign In 클릭 시 SignIn 페이지로 네비게이트
        </li>
      </ul>

      <hr />

      <h3>3. Chapter02 의 두 번째 메뉴에 Sign History 페이지 작성</h3>
      <ul>
        <h4>
          <li>
            로그인, 로그아웃 시 이력을 기록하여 로그인 이력 표시 (이벤트명, ID,
            timestamp)
          </li>
          <li>모든 이력 지우기 기능 구현</li>
        </h4>
      </ul>

      <hr />

      <h3>4. 페이지 이동 시 Scroll To Top 기능 구현</h3>
      <h4>
        개별 페이지의 소스에 구현하는 것이 아닌 한 곳의 소스에서 공통적으로
        적용되게끔 구현
      </h4>

      <hr />
      <h4>Login 상태의 화면</h4>
      <img
        src={loginState}
        alt=""
        style={{ width: "100%", border: "1px solid" }}
      />

      <h4>Logout 상태의 화면</h4>
      <img
        src={logoutState}
        alt=""
        style={{ width: "100%", border: "1px solid" }}
      />

      <h4>이력 화면</h4>
      <img
        src={loginHistory}
        alt=""
        style={{ width: "100%", border: "1px solid" }}
      />
    </div>
  );
};

export default Question02;
