import CodeBlock from "../../components/CodeBlock";

const Question01 = () => {
  return (
    <div>
      <h1>Question 01</h1>
      <h3>
        1. 화면 상단 "KendoReact" 를 현재 열려있는 페이지의 "Chapter명 - 메뉴명"
        으로 변경하세요. ex) Chapter01 - Question01
      </h3>
      <h3>
        2. "Question 01" 페이지의 작성 방법과 동일하게 "Answer 01" 페이지를
        작성하세요.
      </h3>
      <h3>
        3. 하단 question 함수의 "parameter"를 완성시킨 후 "Answer 01" 페이지에
        "CodeBlock Component"를 사용하여 작성하세요.
      </h3>
      <CodeBlock
        language="javascript"
        code={`
const developer = {
  secucen: {
    generalManager: ["박정철", "윤영권"],
    deputyManager: "이미향",
    manager: "김정은",
    clerk: ["김수정", "조성빈"],
  },
  korda: {
    managingDirector: "김형균",
    deputyManager: "유성진",
    assistantManager: "조민준",
    intern: ["여승현", "이준용"],
  },
};

const question = (param) => {
  console.log(\`시큐센의 선배님은 \${clerk} 선배님 입니다.\`); // 김수정
  console.log(\`Korda의 차장님은 \${dM} 입니다.\`); // 유성진
  console.log(\`저는 Korda Intern \${me} 입니다.\`); // 여승현 or 이준용
};

question(developer);
`}
      />
    </div>
  );
};

export default Question01;
