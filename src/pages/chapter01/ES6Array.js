import CodeBlock from "../../components/CodeBlock";

const ES6Array = () => {
  return (
    <div>
      <h1>배열 Array</h1>
      <h3>기본 문법</h3>
      <CodeBlock
        language="javascript"
        code={`
const animalList = ["CAT", "DOG", "TIGER"];
const cat = animalList[0];
const dog = animalList[1];
const tiger = animalList[2];
console.log(cat); // CAT
console.log(dog); // DOG
console.log(tiger); // TIGER

>>>

const [cat, dog, tiger] = ["CAT", "DOG", "TIGER"];
console.log(cat); // CAT
console.log(dog); // DOG
console.log(tiger); // TIGER
`}
      />
      <h3>나머지 문법</h3>
      <CodeBlock
        language="javascript"
        code={`
const animalList = ["CAT", "DOG", "TIGER"];
const [cat, ...restAnimalList] = animalList;
console.log(cat); // CAT
console.log(restAnimalList); // ["DOG", "TIGER"]
`}
      />

      <h3>Default 값 문법</h3>
      <CodeBlock
        language="javascript"
        code={`
const [cat, dog, tiger, monkey = "MONKEY"] = ["CAT", "DOG", "TIGER"];
console.log(cat); // CAT
console.log(dog); // DOG
console.log(tiger); // TIGER
console.log(monkey); // MONKEY
`}
      />

      <h3>다중 구조 문법</h3>
      <CodeBlock
        language="javascript"
        code={`
const developer = [
  [["박정철", "윤영권"], ["이미향"], ["김정은"], ["김수정", "조성빈"]],
  [["김형균"], ["유성진"], ["조민준"], ["여승현", "이준용"]],
];
const [secucen, [kordaMd, ...kordaOther]] = developer;
console.log(secucen); // [["박정철","윤영권"],["이미향"],["김정은"],["김수정","조성빈"]]
console.log(kordaMd); // ["김형균"]
console.log(kordaOther); // [["유성진"],["조민준"],["여승현","이준용"]]
`}
      />
    </div>
  );
};

export default ES6Array;
