import CodeBlock from "../../components/CodeBlock";

const ES6Object = () => {
  return (
    <div>
      <h1>객체 Object</h1>
      <h3>기본 문법</h3>
      <CodeBlock
        language="javascript"
        code={`
const animals = {
  cat: "CAT",
  dog: "DOG",
  tiger: "TIGER"
};
const cat = animals.cat;
const dog = animals.dog;
const tiger = animals.tiger;
console.log(cat); // CAT
console.log(dog); // DOG
console.log(tiger); // TIGER
        
>>>

const { cat, dog, tiger } = {
  cat: "CAT",
  dog: "DOG",
  tiger: "TIGER"
};
console.log(cat); // CAT
console.log(dog); // DOG
console.log(tiger); // TIGER
`}
      />
      <h3>나머지 문법</h3>
      <CodeBlock
        language="javascript"
        code={`
const { cat, ...animals } = {
  cat: "CAT",
  dog: "DOG",
  tiger: "TIGER"
};
console.log(cat); // CAT
console.log(animals); // { dog: DOG, tiger: TIGER }
`}
      />

      <h3>Default 값 문법</h3>
      <CodeBlock
        language="javascript"
        code={`
const { cat, dog, tiger, monkey = "monkey" } = {
  cat: "CAT",
  dog: "DOG",
  tiger: "TIGER"
};
console.log(cat); // CAT
console.log(dog); // DOG
console.log(tiger); // TIGER
console.log(monkey); // MONKEY
`}
      />

      <h3>다중 구조 문법</h3>
      <CodeBlock
        language="javascript"
        code={`
const developer = {
  secucen: {
    generalManager1: "박정철",
    generalManager2: "윤영권",
    deputyManager: "이미향",
    manager: "김정은",
    clerk1: "김수정",
    clerk2: "조성빈",
  },
  korda: {
    managingDirector: "김형균",
    deputyManager: "유성진",
    assistantManager: "조민준",
    intern1: "여승현",
    intern2: "이준용",
  },
};
const {
  secucen,
  korda: { managingDirector: kordaMd, ...kordaOther },
} = developer;
console.log(secucen); // {"generalManager1":"박정철","generalManager2":"윤영권","deputyManager":"이미향","manager":"김정은","clerk1":"김수정","clerk2":"조성빈"}
console.log(kordaMd); // "김형균"
console.log(kordaOther); // {"deputyManager":"유성진","assistantManager":"조민준","intern1":"여승현","intern2":"이준용"}
`}
      />
    </div>
  );
};

export default ES6Object;
